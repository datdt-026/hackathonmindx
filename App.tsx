import React from 'react';
import RootStack from './src/nav/RootStack';

export interface IUserInfo {
  name: string;
  age: string;
  address: string;
}
export interface ITheme {
  backgroundColor: string;
  color: string;
}

const initUserInfo: IUserInfo = {
  name: 'Nguyen Van A',
  age: '20',
  address: 'Ha Noi',
};

export const lightTheme: ITheme = {
  backgroundColor: '#FFF',
  color: '#000',
};

export const darkTheme: ITheme = {
  backgroundColor: '#000',
  color: '#FFF',
};

const initUserContext = {
  userInfo: initUserInfo,
  setUserInfo: (newValue: IUserInfo) => {},
};

const initThemeContext = {
  theme: lightTheme,
  setTheme: (newValue: ITheme) => {},
};

export const UserContext = React.createContext(initUserContext);
export const ThemeContext = React.createContext(initThemeContext);

const App = () => {
  const [userInfo, setUserInfo] = React.useState(initUserInfo);
  const [theme, setTheme] = React.useState(lightTheme);

  const valueUserContext = React.useMemo(
    () => ({userInfo, setUserInfo}),
    [userInfo],
  );
  const valueThemeContext = React.useMemo(() => ({theme, setTheme}), [theme]);

  return (
    <UserContext.Provider value={valueUserContext}>
      <ThemeContext.Provider value={valueThemeContext}>
        <RootStack />
      </ThemeContext.Provider>
    </UserContext.Provider>
  );
};

export default App;
