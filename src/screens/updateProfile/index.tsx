import React from 'react';
import {StyleSheet, Text, View, TextInput, Button} from 'react-native';
import {RootStackParamList} from '../../nav/RootStack';
import {StackNavigationProp} from '@react-navigation/stack';
import {darkTheme, lightTheme, ThemeContext, UserContext} from '../../../App';
import Container from '../../components/Container';
import Txt from '../../components/Txt';

type UpdateProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'UpdateProfile'
>;

type Props = {
  navigation: UpdateProfileScreenNavigationProp;
};
const UpdateProfile = ({navigation}: Props) => {
  const userContext = React.useContext(UserContext);
  const userInfo = userContext.userInfo;
  const setUserInfo = userContext.setUserInfo;
  const themeContext = React.useContext(ThemeContext);
  const isThemeLight = themeContext.theme.backgroundColor === '#FFF';
  const setTheme = themeContext.setTheme;
  return (
    <Container>
      <View>
        <Txt>Tên</Txt>
        <TextInput
          placeholder="Họ và tên"
          defaultValue={userInfo.name}
          onChangeText={(name: string) => {
            setUserInfo({...userInfo, name});
          }}
        />
      </View>
      <View>
        <Txt>Tuổi</Txt>
        <TextInput
          placeholder="Nhập số tuổi"
          defaultValue={userInfo.age}
          onChangeText={(age: string) => {
            setUserInfo({...userInfo, age});
          }}
        />
      </View>

      <Button
        title="Cập nhật"
        onPress={() => {
          navigation.goBack();
        }}
      />
      <Button
        title="Cập nhật Theme"
        onPress={() => {
          if (isThemeLight) {
            setTheme(darkTheme);
          } else {
            setTheme(lightTheme);
          }
        }}
      />
    </Container>
  );
};

export default UpdateProfile;

const styles = StyleSheet.create({});
