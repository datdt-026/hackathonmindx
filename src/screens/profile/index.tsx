import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {RootStackParamList} from '../../nav/RootStack';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {UserContext} from '../../../App';
import Container from '../../components/Container';
import Txt from '../../components/Txt';

export interface IUserInfo {
  name: string;
  age: string;
}
type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Profile'
>;

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Profile'>;
type Props = {
  navigation: ProfileScreenNavigationProp;
  route: ProfileScreenRouteProp;
};

const Profile = React.memo(({navigation, route}: Props) => {
  const userContext = React.useContext(UserContext);
  const userInfo = userContext.userInfo;
  console.log('Profile', userInfo);
  return (
    <Container>
      <Txt>{`Ten: ${userInfo.name}`}</Txt>
      <Txt>{`Age: ${userInfo.age}`}</Txt>

      <Button
        title="Cập nhật thông tin"
        onPress={() => {
          navigation.navigate('UpdateProfile', {
            name: userInfo.name,
            age: userInfo.age,
          });
        }}
      />
    </Container>
  );
});

export default Profile;

const styles = StyleSheet.create({});
